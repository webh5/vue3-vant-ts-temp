/*
 * @Author       : yinzhishan
 * @Date         : 2020-12-28 16:23:35
 * @FilePath     : /appp/babel.config.js
 * @Description  : Do not edit
 */
module.exports = {
  plugins: [
    [
      "import",
      {
        libraryName: "vant",
        libraryDirectory: "es",
        style: true
      },
      "vant"
    ]
  ]
};
