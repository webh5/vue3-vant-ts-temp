/*
 * @Author       : yinzhishan
 * @Date         : 2020-12-28 16:01:09
 * @FilePath     : /appp/src/main.ts
 * @Description  : Do not edit
 */
import { createApp } from "vue";
import App from "./App.vue";
import router, {setupRouter} from './router/'
import {setupStore} from '@/store'

import {setupVant, setupDirectives, setupGlobalMethods,setupCustomComponents} from '@/plugins/'

// rem h5 适配
import "amfe-flexible/index.js";

const app = createApp(App)

setupVant(app);
// 注册全局自定义组件,
setupCustomComponents(app);
// 注册全局自定义指令，如：v-permission权限指令
setupDirectives(app);
// 注册全局方法，如：app.config.globalProperties.$message = message
setupGlobalMethods(app);
// 挂载vuex状态管理
setupStore(app)
// 挂载路由
setupRouter(app)
// 路由准备就绪后挂载APP实例
router.isReady().then(() => app.mount('#app'))
