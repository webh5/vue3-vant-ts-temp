/*
 * @Author       : yinzhishan
 * @Date         : 2020-12-29 12:11:30
 * @FilePath     : /appp/src/enums/cacheEnum.ts
 * @Description  : Do not edit
 */
// token key
export const TOKEN_KEY = 'TOKEN';

// user info key
export const USER_INFO_KEY = 'USER__INFO__';

