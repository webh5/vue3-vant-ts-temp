/*
 * @Author       : yinzhishan
 * @Date         : 2020-12-29 12:21:51
 * @FilePath     : /appp/src/store/index.ts
 * @Description  : Do not edit
 */
import { createStore } from "vuex";
import { App } from "vue";

const store = createStore({
  state: {
    testName: "hello"
  },
  mutations: {},
  actions: {}
});

export function setupStore(app: App) {
  app.use(store);
  console.log(store, "vuex");
}

export default store;
