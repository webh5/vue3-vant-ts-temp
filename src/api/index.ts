/*
 * @Author       : yinzhishan
 * @Date         : 2020-12-29 14:14:00
 * @FilePath     : /appp/src/api/index.ts
 * @Description  : Do not edit
 */
import http from '@/utils/http/axios';
import {RequestEnum} from '@/enums/httpEnum'



enum Api {
  getArticleInfo = '/on_star_service_http/custom/api/http/getArticleInfo.do',
}

/**
 * @description: 获取文章列表
 */
export function getArticleInfo(params?:object) {
  return http.request({
    url: Api.getArticleInfo,
    method: RequestEnum.POST,
    params
  });
}


