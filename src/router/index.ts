/*
 * @Author       : yinzhishan
 * @Date         : 2020-12-29 12:21:51
 * @FilePath     : /appp/src/router/index.ts
 * @Description  : Do not edit
 */
import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/Home.vue";

import { App } from "vue";

export const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
];

const router = createRouter({
  // process.env.BASE_URL
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export function setupRouter(app: App) {
  app.use(router);
  // 创建路由守卫
}
export default router;
