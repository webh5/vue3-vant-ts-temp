/*
 * @Author       : yinzhishan
 * @Date         : 2020-12-29 12:12:09
 * @FilePath     : /appp/src/plugins/globalMethods.ts
 * @Description  : Do not edit
 */
import { App } from "vue";

// import hasPermission from "@/utils/permission/hasPermission";

/**
 * 注册全局方法
 * @param app
 */
export function setupGlobalMethods(app: App) {
  // app.use(hasPermission)
}
