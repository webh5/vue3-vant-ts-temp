/*
 * @Author       : yinzhishan
 * @Date         : 2020-12-29 12:12:09
 * @FilePath     : /appp/src/plugins/vant.ts
 * @Description  : Do not edit
 */
import type { App } from 'vue';

import { Button, NumberKeyboard } from "vant";
import "vant/lib/index.css";

export function setupVant(app: App<Element>) {
  app.use(Button)
    .use(NumberKeyboard)
}
