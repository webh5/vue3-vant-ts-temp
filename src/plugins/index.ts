/*
 * @Author       : yinzhishan
 * @Date         : 2020-12-29 12:12:09
 * @FilePath     : /appp/src/plugins/index.ts
 * @Description  : Do not edit
 */
export { setupVant } from "@/plugins/vant";
export { setupDirectives } from "@/plugins/directives";
export { setupCustomComponents } from "@/plugins/customComponents";
export { setupGlobalMethods } from "@/plugins/globalMethods";
