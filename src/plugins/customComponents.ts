/*
 * @Author       : yinzhishan
 * @Date         : 2020-12-29 12:12:09
 * @FilePath     : /appp/src/plugins/customComponents.ts
 * @Description  : Do not edit
 */
import { App } from "vue";

// import {SvgIcon} from '@/components/svg-icon'

/**
 * 全局注册自定义组件
 * @param app
 */
export function setupCustomComponents(app: App) {
  // app.component(SvgIcon.name, SvgIcon)
}
