/*
 * @Author       : yinzhishan
 * @Date         : 2020-10-12 10:12:55
 * @FilePath     : /app/vue.config.js
 * @Description  : Do not edit
 */
const path = require("path");
// "fibers": "^3.1.1",
module.exports = {
  devServer: {
    // host: '0.0.0.0', // can be overwritten by process.env.HOST
    // 设置默认端口
    port: 8080,
    // 设置代理
    proxy: {
      "/api": {
        // 目标 API 地址
        target: "http://192.168.0.1:8080",
        changeOrigin: true,
        pathRewrite: {
          "^/api": ""
        }
      }
    }
  },

  // css相关配置
  css: {
    // 是否使用css分离插件 ExtractTextPlugin
    extract: true,
    // 开启 CSS source maps?
    sourceMap: false
  }
};
