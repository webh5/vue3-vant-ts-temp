/*
 * @Author       : yinzhishan
 * @Date         : 2020-12-28 16:36:42
 * @FilePath     : /appp/.postcssrc.js
 * @Description  : Do not edit
 */
module.exports = {
  plugins: {
    //...
    "postcss-pxtorem": {
      rootValue: 37.5, //vant-UI的官方根字体大小是37.5
      propList: ["*"],
      selectorBlackList: [".placehloder-image", ".placehloderImg"]
    }
  }
};
